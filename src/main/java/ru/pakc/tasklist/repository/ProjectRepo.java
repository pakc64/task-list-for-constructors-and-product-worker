package ru.pakc.tasklist.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pakc.tasklist.model.Project;

public interface ProjectRepo extends JpaRepository<Project, Long> {
    Project findProjectByDesignation();

    Project findProjectByName();

    Project findProjectByCreateData();
}

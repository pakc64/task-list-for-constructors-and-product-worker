package ru.pakc.tasklist.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.pakc.tasklist.model.User;

@Repository
public interface UserRepo extends JpaRepository<User, Long> {



}

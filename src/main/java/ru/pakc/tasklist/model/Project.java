package ru.pakc.tasklist.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Entity
@Component
@AllArgsConstructor
@NoArgsConstructor
public class Project {

    @Id
    Long id;
    String designation;
    String createData;
    String condition;
    String wordDate;

}

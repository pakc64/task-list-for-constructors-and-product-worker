package ru.pakc.tasklist.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import ru.pakc.tasklist.model.User;

@Controller
public class MainController {

    @GetMapping("/")
    public String logIn() {
        return "log_in";
    }

    @PostMapping("/check-user")
    public String logIn(User user, Model model) {

        return "redirect:/constructor";
    }

    @GetMapping("/constructor")
    public String constructorPage() {
        return "constructor_page";
    }
}
